def getNumbers() -> int | bool:
    try:
        keep = True
        num = int(input("Digite um número (digite qualquer negativo para parar o programa) -> "))
        if num < 0:
            keep = False
            num = None
        return [num, keep]
    except ValueError:
        print("Por favor, insira um número!")
        return None, True

nums = []
keep = True

while keep:
    numb, keep = getNumbers()
    nums.extend([numb] if numb != None else [])

print (f"\nNúmeros digitados por você: {nums}\n")
#low to high
nums.sort()
print (f"Números do menor pro maior: {nums}")
#high to low
nums.sort(reverse=True)
print (f"Números do maior pro menor: {nums}")
#lowest
print (f"O menor número foi: {nums[len(nums)-1]}")
#highest
print (f"O maior número foi: {nums[0]}")